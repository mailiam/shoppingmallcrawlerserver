/**
 * Module dependencies.
 */

var fs = require('fs')
var express = require('express');
var app = express();

var args = process.argv.join('\n');
var portMatch = args.match(/--port\s*=\s*(.*)/mi);
var port = portMatch ? portMatch[1] : 8008;

var stripComment = true;

app.get('/:path*', function (req, res){
	fs.readFile('./src/'+req.path+'.js', function (err,data) {
	  if (err) {
	    res.status(404).send('Api not found');
	  }
	  res.set('Content-Type', 'application/javascript; charset=utf-8'); 
	  if(stripComment){
	  	data = String(data).replace(/(\/\*[\s\S]*\*\/\n*|[^:]\/\/.*)/g,'');
	  }
	  res.send(data);
	});
});

app.listen(port, function () {
	console.log('listening on port '+port);
});
