/**
 * @module           Gmarket
 * @todo       주문 후 배송정보 추가
 */

/**
 * G마켓 로그인
 * http://mobile.gmarket.co.kr/Login/Login?URL=http://mmyg.gmarket.co.kr/home
 *
 * @version    0.1.0
 *
 * @method     login
 * @param      {string}  id      login id
 * @param      {string}  pw      login password
 * @return     {Error}   페이지가 예상한것과 다를 경우 에러
 */
function login(id, pw){
  try{
    var loginForm = document.querySelector('form#defaultForm');
    loginForm.querySelector('input#id').value=id;
    loginForm.querySelector('input#pwd').value=pw;
    loginForm.querySelector('button.lgs').click();
  }catch(e){
    return e;
  }
}

/**
 * G마켓 로그인 확인
 *
 * @version    0.1.0
 *
 * @method     checkLogin
 * @return     {bool}  로그인이 되었으면 true를 반환한다.
 */
function checkLogin(){
	try{
		var loginLogoutButton = document.querySelector('#foot .btn_lf a span');
		if(loginLogoutButton){ //Explicitly check for unexpected page update
			switch(loginLogoutButton.innerText){
				case '로그아웃':
					return true;
				case '로그인':
					return false;
				default:
					throw new Error('Login Check Failed');
			}
		}
		throw new Error('Login Check Failed');
	}catch(e){
		return e;
	}
}

/**
 * G마켓 주문 목록 조회 http://mmyg.gmarket.co.kr/
 *
 * @version    0.1.0
 *
 * @method     getOrders
 * @return     {Object[]}  주문 목록들
 */
function getOrders() {
	try { 
		var list = document.querySelector('#form0').querySelectorAll('.mybuy_list');  //Explictly check #form0 for gmarket orderlist page
		var groupedDateElement = null;
		var groupedOrderDate = null;
		var groupedDetailElement = null;
		var groupedDetailUrl = null;
		var orders = Array.prototype.map.call(list, function(dom) {
			if(groupedDateElement = dom.querySelector('.date'))
				groupedOrderDate = groupedDateElement.innerText;
			if(groupedDetailElement = dom.querySelector('.more'))
				groupedDetailUrl = groupedDetailElement.href;
			var order = {
				orderDate: new Date(groupedOrderDate),
				orderNumber: dom.querySelector('.tx_num').innerText.replace(/\D/g,''),
				detailUrl: groupedDetailUrl,
				trackingUrl: null,
				trackingNumber: null, 	// accessible through trackingUrl
				carrier: null,			// accessible through trackingUrl
				image: dom.querySelector('.img').src,
				name: dom.querySelector('.prod_nm').innerText,
				status: dom.querySelector('.btm_al').innerText,
				totalPrice: document.querySelector('#form0').querySelector('.mybuy_list').querySelector('.c_price em:last-of-type').innerText.replace(/\D/g,''),
				seller: {
					name: dom.querySelector('.shop_nm').innerText,
					tel: dom.querySelector('.phone').href.replace(/\D/g, '')
				}
			}
			return order;
		});
		return orders;
	}catch(e){
		return e;
	}
}
