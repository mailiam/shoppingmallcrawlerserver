/**
 * 옥션 로그인
 * http://m.11st.co.kr/MW/Login/login.tmall?returnURL=http%253A%252F%252Fm.11st.co.kr%252FMW%252FMyPage%252ForderList.tmall
 *
 * @version    0.1.0
 * @method     login
 * @module               Auction
 * @param      {string}  id      login id
 * @param      {string}  pw      login password
 * @return     {Error}   페이지가 예상한것과 다를 경우 에러
 */
function login(id, pw){
  try{
    var loginForm = document.querySelector('div#hdivLoginForm');
    loginForm.querySelector('input#id').value=id;
    loginForm.querySelector('input#password').value=pw;
    loginForm.querySelector('button').click();
  }catch(e){
    return null;
  }
}
