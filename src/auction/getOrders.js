/**
 * 옥션 주문 목록 조회 https://mmya.auction.co.kr/MyAuction/Order/#/OrderList
 *
 * @version    0.1.0
 * @method     getOrders
 * @module                 Auction
 * @return     {Object[]}  주문 목록들
 */
function getOrders() {
	var excludingStatuses = ['환불'];
	var excludingStatusRegex = new RegExp('(?:'+excludingStatuses.join('|')+')', 'i');
	try { 
		var payments = angular.element('.order-products-list').scope().ui.orderList.PayInfoList
		var orders = []; 
		for(var paymentIndex = 0; paymentIndex < payments.length; paymentIndex++){
			var payment = payments[paymentIndex];
			var paymentOrderList = payment.OrderList;
			for(var orderIndex = 0; orderIndex < paymentOrderList.length; orderIndex++){
				var paymantOrder = paymentOrderList[orderIndex];
				var order = {
					orderDate: new Date(payment.PayDateString),
					orderNumber: paymantOrder.OrderNo,
					detailUrl: 'https://mmya.auction.co.kr/MyAuction/Order/#/MyOrderDetail/'+payment.PayNo+'/'+orderIndex,
					trackingUrl: paymantOrder.DisplayOption.DeliveryTrackingDisplay? 'http://mobile.auction.co.kr/MyAuction/TraceItem.aspx?orderNo='+paymantOrder.OrderNo : null,
					trackingNumber: null, 
					carrier: null,
					image: paymantOrder.ItemImageUrl,
					name: paymantOrder.ItemName,
					status: paymantOrder.Status,
					price: {
						item: paymantOrder.ItemPrice,
						shipping: paymantOrder.ShippingFee
					},
					totalPrice: payment.TotalOrderAmount,
					seller: {
						name: paymantOrder.SellerNickName,
						tel: paymantOrder.SellerTelNumber,
						mobile: paymantOrder.SellerMobileTelNo,
						email: paymantOrder.SellerEmail,
						address: paymantOrder.SellerAddress
					}
				}
				var excluding = order.status.match(excludingStatusRegex);
				if(!excluding) orders.push(order);
			}
		}
		return orders;
	}catch(e){
		return null;
	}
}