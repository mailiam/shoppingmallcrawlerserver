/**
 * 옥션 로그인 확인
 *
 * @version    0.1.0
 * @method     checkLogin
 * @module             Auction
 * @return     {bool}  로그인이 되었으면 true를 반환한다.
 */
function checkLogin(){
	try{
		var loginLogoutButton = document.querySelector('li.foot-item a')
		if(loginLogoutButton){ //Explicitly check for unexpected page update
			switch(loginLogoutButton.text){
				case '로그아웃':
					return true;
				case '로그인':
					return false;
				default:
					throw new Error('Login Check Failed');
			}
		}
		throw new Error('Login Check Failed');
	}catch(e){
		return null;
	}
}
