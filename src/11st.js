/**
 * @module           11st
 */

/**
 * 11번가 로그인
 * http://m.11st.co.kr/MW/Login/login.tmall?returnURL=http%253A%252F%252Fm.11st.co.kr%252FMW%252FMyPage%252ForderList.tmall
 *
 * @version    0.1.0
 *
 * @method     login
 * @param      {string}  id      login id
 * @param      {string}  pw      login password
 * @return     {Error}   페이지가 예상한것과 다를 경우 에러
 */
function login(id, pw){
  try{
    var loginForm = document.querySelector('form#loginform');
    loginForm.querySelector('input#userId').value=id;
    loginForm.querySelector('input#userPw').value=pw;
    loginForm.querySelector('button#loginButton').click();
  }catch(e){
    return e;
  }
}

/**
 * 11번가 로그인 확인
 *
 * @version    0.1.0
 *
 * @method     checkLogin
 * @return     {bool}  로그인이 되었으면 true를 반환한다.
 */
function checkLogin(){
	try{
		return funcCheckIsLogin();
	}catch(e){
		return e;
	}
}

/**
 * 11번가 주문 목록 조회 http://m.11st.co.kr/MW/MyPage/orderList.tmall
 *
 * @version    0.1.0
 *
 * @method     getOrders
 * @return     {Object[]}  주문 목록들
 */
function getOrders() {
	try { 
		/*eslint-env es6*/
		var list = document.querySelector('#dataListAjax').querySelectorAll('section.odr_rbox');  //Explictly check #dataListAjax for 11st orderlist page
		var orders = Array.prototype.map.call(list, function(dom) {
			var dnTexts = dom.querySelector('.odr_dn .dn').innerText.split(/\s+/); //[orderDate, ordernumber]
			var sellerTexts = dom.querySelector('.seller').innerText.split(/\s+/);
			var prices = dom.querySelectorAll('.info ul li b:first-of-type');
			var totalPrice = Array.prototype.reduce.call(prices, function (prev, current) {
				current = parseFloat(current.innerText.replace(',',''));
				return current + prev;
			}, 0);
			var trackingButton = Array.prototype.filter.call(dom.querySelectorAll('.prd_p .info .btn a'),(element, index, array)=>{return element.text == '배송추적'})[0];

			var order = {
				orderDate: new Date(dnTexts[0]),
				orderNumber: dnTexts[1],
				detailUrl: dom.querySelector('.odr_dn .dn a').href,
				trackingUrl: trackingButton? trackingButton.href : null,
				trackingNumber: null, 	// accessible through trackingUrl
				carrier: null,			// accessible through trackingUrl
				image: dom.querySelector('.prd_p .thumb img').src,
				name: dom.querySelector('.prd_p .name').innerText,
				status: dom.querySelector('.prd_p .rt').innerText.replace(/\s*$/,''),
				totalPrice: totalPrice,
				seller: {
					name: sellerTexts[0],
					tel:sellerTexts[1]
				}
			}
			return order;
		});
		return orders;
	}catch(e){
		return e;
	}
}

/**
 * 11번가 배송 조회 http://m.11st.co.kr/MW/MyPage/orderSendInfo.tmall
 *
 * @version    0.1.0
 *
 * @method     getTrackingInfo
 * @return     {Object | Error}  운송사와 운송장번호, 페이지가 예상한 것과 다르면 에러
 */
function getTrackingInfo() {
	try{
		var infoList = document.querySelectorAll('#cts.my_cts .ship_info li'); //[carrier, trackingNumber]
		var order = {
			carrier: infoList[0].querySelector('a.homepage').text,
			trackingNumber: infoList[1].innerText.replace('송장번호',''),
		}
		return order;
	}catch(e){
		return e;
	}
}