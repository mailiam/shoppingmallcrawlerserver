/**
 * G마켓 로그인
 * http://mobile.gmarket.co.kr/Login/Login?URL=http://mmyg.gmarket.co.kr/home
 *
 * @version    0.1.0
 * @method     login
 * @module               GMarket
 * @param      {string}  id      login id
 * @param      {string}  pw      login password
 * @return     {Error}   페이지가 예상한것과 다를 경우 에러
 */
function login(id, pw){
  try{
    var loginForm = document.querySelector('form#defaultForm');
    loginForm.querySelector('input#id').value=id;
    loginForm.querySelector('input#pwd').value=pw;
    loginForm.querySelector('button.lgs').click();
  }catch(e){
    return null;
  }
}
