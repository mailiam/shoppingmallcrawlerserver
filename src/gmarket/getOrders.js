/**
 * G마켓 주문 목록 조회 
 * http://mmyg.gmarket.co.kr/
 *
 * @version    0.1.0
 * @method     getOrders
 * @module                 Gmarket
 * @return     {Object[]}  주문 목록들
 */
function getOrders() {
	try { 
		var list = document.querySelector('#form0').querySelectorAll('.mybuy_list');  //Explictly check #form0 for gmarket orderlist page
		var groupedDateElement = null;
		var groupedOrderDate = null;
		var groupedDetailElement = null;
		var groupedDetailUrl = null;
		var orders = Array.prototype.map.call(list, function(dom) {
			if(groupedDateElement = dom.querySelector('.date'))
				groupedOrderDate = groupedDateElement.innerText;
			if(groupedDetailElement = dom.querySelector('.more'))
				groupedDetailUrl = groupedDetailElement.href;
			var order = {
				orderDate: new Date(groupedOrderDate),
				orderNumber: dom.querySelector('.tx_num').innerText.replace(/\D/g,''),
				detailUrl: groupedDetailUrl,
				/// @todo 배송정보 추가
				trackingUrl: null,
				trackingNumber: null, 	// accessible through trackingUrl
				carrier: null,			// accessible through trackingUrl
				image: dom.querySelector('.img').src,
				name: dom.querySelector('.prod_nm').innerText,
				status: dom.querySelector('.btm_al').innerText,
				totalPrice: document.querySelector('#form0').querySelector('.mybuy_list').querySelector('.c_price em:last-of-type').innerText.replace(/\D/g,''),
				seller: {
					name: dom.querySelector('.shop_nm').innerText,
					tel: dom.querySelector('.phone').href.replace(/\D/g, '')
				}
			}
			return order;
		});
		return orders;
	}catch(e){
		return null;
	}
}
