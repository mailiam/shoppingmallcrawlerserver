/**
 * G마켓 로그인 확인
 *
 * @version    0.1.0
 * @method     checkLogin
 * @module             GMarket
 * @return     {bool}  로그인이 되었으면 true를 반환한다.
 */
function checkLogin(){
	try{
		var loginLogoutButton = document.querySelector('#foot .btn_lf a span');
		if(loginLogoutButton){ //Explicitly check for unexpected page update
			switch(loginLogoutButton.innerText){
				case '로그아웃':
					return true;
				case '로그인':
					return false;
				default:
					throw new Error('Login Check Failed');
			}
		}
		throw new Error('Login Check Failed');
	}catch(e){
		return null;
	}
}
