/**
 * @module           Wemakeprice
 */

/**
 * 위메프 로그인
 * https://member.wemakeprice.com/member/login/?f=0&rurl=http://wemakeprice.com%2Fmypage%2Fbuylist%2Fdelivery_list&
 *
 * @warning 	모바일 웹사이트 데이터가 빈약하여 데스크탑 웹사이트에서 작동
 *
 * @version    0.1.0
 *
 * @method     login
 * @param      {string}  id      login id
 * @param      {string}  pw      login password
 * @return     {Error}   페이지가 예상한것과 다를 경우 에러
 */
function login(id, pw){
  try{
    var loginForm = document;
    loginForm.querySelector('input#login_uid').value=id;
    loginForm.querySelector('input#login_upw').value=pw;
    loginForm.querySelector('button#loginConfirmBtn').click();
  }catch(e){
    return e;
  }
}

/**
 * 위메프 로그인 확인
 *
 * @warning 	모바일 웹사이트 데이터가 빈약하여 데스크탑 웹사이트에서 작동
 *
 * @version    0.1.0
 *
 * @method     checkLogin
 * @return     {bool}  로그인이 되었으면 true를 반환한다.
 */
function checkLogin(){
	try{
		return js_isLogin === '1';
		throw new Error('Login Check Failed');
	}catch(e){
		return e;
	}
}

/**
 * 위메프 주문 목록 조회 http://wemakeprice.com/mypage/buylist/delivery_list
 *
 * @version    0.1.0
 *
 * @method     getOrders
 * @return     {Object[]}  주문 목록들
 */
function getOrders() {
	try { 
		var trackingUrlRegex = new RegExp(/'(\/mypage.*\/)'/); //extract trackingUrl from function
		var list = document.querySelector('table.tbl_mypage').querySelectorAll('tbody tr');
		var orders = Array.prototype.map.call(list, function(dom) {
			var trackingButton = dom.querySelector('a.btn_flexible02');
			var dateAndOrderNumberTexts = dom.querySelector('.cont.fst').innerText.split('\n'); //[date, orderNumber, buttons]
			var order = {
				orderDate: new Date(dateAndOrderNumberTexts[0]),
				orderNumber: dateAndOrderNumberTexts[1].replace(/\D/g,''),
				detailUrl: dom.querySelector('.cont.fst a.btn_flexible01').href,
				trackingUrl: trackingButton ? 'http://wemakeprice.com'+dom.querySelector('a.btn_flexible02').onclick.toString().match(trackingUrlRegex)[1] : null,
				trackingNumber: null, 	// accessible through trackingUrl
				carrier: null,			// accessible through trackingUrl
				image: dom.querySelector('img').src,
				name: dom.querySelector('.tit').innerText,
				status: dom.querySelector('td:last-child').innerText.split(/\s/)[0],
				totalPrice: dom.querySelector('.num').innerText.replace(/\D/g,''),
				seller: null
			}
			return order;
		});
		return orders;
	}catch(e){
		return e;
	}
}

/**
 * 위메프 배송 조회 http://wemakeprice.com/mypage/delivery_popup/
 *
 * @warning 	모바일 웹사이트 데이터가 빈약하여 데스크탑 웹사이트에서 작동
 *
 * @version    0.1.0
 *
 * @method     getTrackingInfo
 * @return     {Object | Error}  운송사와 운송장번호, 페이지가 예상한 것과 다르면 에러
 */
function getTrackingInfo() {
	try{
		//disable redirection
		window.open = function(url){
			return false;
		}

		var order = {
			carrier: document.querySelector('tbody td:last-child p').innerText,
			trackingNumber: document.querySelector('tbody td:last-child a').innerText,
		}
		return order;
	}catch(e){
		return e;
	}
}

