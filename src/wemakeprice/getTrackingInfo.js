/**
 * 위메프 배송 조회 http://wemakeprice.com/mypage/delivery_popup/
 *
 * @warning 	모바일 웹사이트 데이터가 빈약하여 데스크탑 웹사이트에서 작동 
 *
 * @version    0.1.0
 * @method     getTrackingInfo
 * @module     WeMakePrice
 * @return     {Object | Error}  운송사와 운송장번호, 페이지가 예상한 것과 다르면 에러
 */
function getTrackingInfo() {
	try{
		//disable redirection
		window.open = function(url){
			return false;
		}

		var order = {
			carrier: document.querySelector('tbody td:last-child p').innerText,
			trackingNumber: document.querySelector('tbody td:last-child a').innerText,
		}
		return order;
	}catch(e){
		return null;
	}
}
