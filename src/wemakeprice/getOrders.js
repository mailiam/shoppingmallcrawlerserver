/**
 * 위메프 주문 목록 조회 http://wemakeprice.com/mypage/buylist/delivery_list
 *
 * @version    0.1.0
 * @method     getOrders
 * @module                 WeMakePrice
 * @return     {Object[]}  주문 목록들
 */
function getOrders() {
	try { 
		var trackingUrlRegex = new RegExp(/'(\/mypage.*\/)'/); //extract trackingUrl from function
		var list = document.querySelector('table.tbl_mypage').querySelectorAll('tbody tr');
		var orders = Array.prototype.map.call(list, function(dom) {
			var trackingButton = dom.querySelector('a.btn_flexible02');
			var dateAndOrderNumberTexts = dom.querySelector('.cont.fst').innerText.split('\n'); //[date, orderNumber, buttons]
			var order = {
				orderDate: new Date(dateAndOrderNumberTexts[0]),
				orderNumber: dateAndOrderNumberTexts[1].replace(/\D/g,''),
				detailUrl: dom.querySelector('.cont.fst a.btn_flexible01').href,
				trackingUrl: trackingButton ? 'http://wemakeprice.com'+dom.querySelector('a.btn_flexible02').onclick.toString().match(trackingUrlRegex)[1] : null,
				trackingNumber: null, 	// accessible through trackingUrl
				carrier: null,			// accessible through trackingUrl
				image: dom.querySelector('img').src,
				name: dom.querySelector('.tit').innerText,
				status: dom.querySelector('td:last-child').innerText.split(/\s/)[0],
				totalPrice: dom.querySelector('.num').innerText.replace(/\D/g,''),
				seller: null
			}
			return order;
		});
		return orders;
	}catch(e){
		return null;
	}
}
