/**
 * 위메프 로그인
 * https://member.wemakeprice.com/member/login/?f=0&rurl=http://wemakeprice.com%2Fmypage%2Fbuylist%2Fdelivery_list&
 *
 * @warning 	모바일 웹사이트 데이터가 빈약하여 데스크탑 웹사이트에서 작동 
 *
 * @version    0.1.0
 * @method     login
 * @module     WeMakePrice
 * @param      {string}  id      login id
 * @param      {string}  pw      login password
 * @return     {Error}   페이지가 예상한것과 다를 경우 에러
 */
function login(id, pw){
  try{
    var loginForm = document;
    loginForm.querySelector('input#login_uid').value=id;
    loginForm.querySelector('input#login_upw').value=pw;
    loginForm.querySelector('button#loginConfirmBtn').click();
  }catch(e){
    return null;
  }
}
