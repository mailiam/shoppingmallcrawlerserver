/**
 * 위메프 로그인 확인
 *
 * @warning 	모바일 웹사이트 데이터가 빈약하여 데스크탑 웹사이트에서 작동
 *
 * @version    0.1.0
 * @method     checkLogin
 * @module             WeMakePrice
 * @return     {bool}  로그인이 되었으면 true를 반환한다.
 */
function checkLogin(){
	try{
		return js_isLogin === '1';
		throw new Error('Login Check Failed');
	}catch(e){
		return null;
	}
}
