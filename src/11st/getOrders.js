/**
 * 11번가 주문 목록 조회 http://m.11st.co.kr/MW/MyPage/orderList.tmall
 *
 * @version    0.1.0
 * @method     getOrders
 * @module                 11st
 * @return     {Object[]}  주문 목록들
 */
function getOrders() {
	try { 
		var list = document.querySelector('#dataListAjax').querySelectorAll('section.odr_rbox');  //Explictly check #dataListAjax for 11st orderlist page
		var orders = Array.prototype.map.call(list, function(dom) {
			var dnTexts = dom.querySelector('.odr_dn .dn').innerText.split(/\s+/); //[orderDate, ordernumber]
			var sellerTexts = dom.querySelector('.seller').innerText.split(/\s+/);
			var prices = dom.querySelectorAll('.info ul li b:first-of-type');
			var totalPrice = Array.prototype.reduce.call(prices, function (prev, current) {
				current = parseFloat(current.innerText.replace(',',''));
				return current + prev;
			}, 0);
			var trackingButton = Array.prototype.filter.call(dom.querySelectorAll('.prd_p .info .btn a'), function(element, index, array){return element.text == '배송추적'})[0];

			var order = {
				orderDate: new Date(dnTexts[0]),
				orderNumber: dnTexts[1],
				detailUrl: dom.querySelector('.odr_dn .dn a').href,
				trackingUrl: trackingButton? trackingButton.href : null,
				trackingNumber: null, 	// accessible through trackingUrl
				carrier: null,			// accessible through trackingUrl
				image: dom.querySelector('.prd_p .thumb img').src,
				name: dom.querySelector('.prd_p .name').innerText,
				status: dom.querySelector('.prd_p .rt').innerText.replace(/\s*$/,''),
				totalPrice: totalPrice,
				seller: {
					name: sellerTexts[0],
					tel:sellerTexts[1]
				}
			}
			return order;
		});
		return orders;
	}catch(e){
		return null;
	}
}
