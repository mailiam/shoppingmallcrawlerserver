/**
 * 11번가 로그인 확인
 *
 * @version    0.1.0
 * @method     checkLogin
 * @module             11st
 * @return     {bool}  로그인이 되었으면 true를 반환한다.
 */
function checkLogin(){
	try{
		return funcCheckIsLogin();
	}catch(e){
		return null;
	}
}
