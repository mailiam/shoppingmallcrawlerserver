/**
 * 11번가 배송 조회 http://m.11st.co.kr/MW/MyPage/orderSendInfo.tmall
 *
 * @version    0.1.0
 * @method     getTrackingInfo
 * @module                       11st
 * @return     {Object | Error}  운송사와 운송장번호, 페이지가 예상한 것과 다르면 에러
 */
function getTrackingInfo() {
	try{
		var infoList = document.querySelectorAll('#cts.my_cts .ship_info li'); //[carrier, trackingNumber]
		var order = {
			carrier: infoList[0].querySelector('a.homepage').text,
			trackingNumber: infoList[1].innerText.replace('송장번호',''),
		}
		return order;
	}catch(e){
		return null;
	}
}
