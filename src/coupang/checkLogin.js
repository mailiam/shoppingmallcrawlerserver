/**
 * 쿠팡 로그인 확인
 *
 * @version    0.1.0
 * @method     checkLogin
 * @module             Coupang
 * @return     {bool}  로그인이 되었으면 true를 반환한다.
 */
function checkLogin(){
	try{
		return document.querySelector('#logoutBtn, #loginBtn').innerText === '로그아웃';
		throw new Error('Login Check Failed');
	}catch(e){
		return null;
	}
}
