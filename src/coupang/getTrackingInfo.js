/**
 * 쿠팡 배송 조회 http://b2c.goodsflow.com/small/Coupang/TrackingList/Old2.aspx
 *
 * @version    0.1.0
 * @method     getTrackingInfo
 * @module                       Coupang
 * @return     {Object | Error}  운송사와 운송장번호, 페이지가 예상한 것과 다르면 에러
 */
function getTrackingInfo() {
	try{
		var infoList = document.querySelectorAll('.delivery-company_info li strong'); //[carrier, trackingNumber]
		var order = {
			carrier: infoList[0].innerText,
			trackingNumber: infoList[1].innerText,
		}
		return order;
	}catch(e){
		return null;
	}
}
