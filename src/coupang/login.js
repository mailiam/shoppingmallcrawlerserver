/**
 * 쿠팡 로그인
 * https://login.coupang.com/login/login.pang?rtnUrl=https%3A%2F%2Fmy.coupang.com%2Fm%2Fpurchase%2FlistByDelivery
 *
 * @version    0.1.0
 * @method     login
 * @module               Coupang
 * @param      {string}  id      login id
 * @param      {string}  pw      login password
 * @return     {Error}   페이지가 예상한것과 다를 경우 에러
 */
function login(id, pw){
  try{
    var loginForm = document.querySelector('form#loginForm');
    loginForm.querySelector('input#login-tf-email').value=id;
    loginForm.querySelector('input#login-tf-password').value=pw;
    loginForm.querySelector('button#login-submit').click();
  }catch(e){
    return null;
  }
}
