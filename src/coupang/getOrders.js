/**
 * 쿠팡 주문 목록 조회 https://my.coupang.com/m/purchase/listByDelivery
 *
 * @version    0.1.0
 * @method     getOrders
 * @module                 Coupang
 * @return     {Object[]}  주문 목록들
 */
function getOrders() {
	var excludingStatuses = ['취소'];
	var excludingStatusRegex = new RegExp('(?:'+excludingStatuses.join('|')+')', 'i');
	
	try { 
		var trackingUrlRegex = new RegExp(/'(\/mypage.*\/)'/); //extract trackingUrl from function
		var list = document.querySelector('#listContainer').querySelectorAll('#listContainer > div');
		var orders = Array.prototype.map.call(list, function(dom) {
			var trackingButton = dom.querySelector('button.deliveryTracking');
			var trackingInfo = trackingButton ? JSON.parse(trackingButton.dataset.delivery) : null;
			var trackingUrl = trackingInfo ? trackingInfo.url : null;
			var trackingNumber = trackingUrl ? trackingUrl.match(/sheet_no=(\d*)/)[1] : null;
			var carrier = trackingInfo ? trackingInfo.coupangDelivery ? '로켓배송' : null : null;

			var detailUrl = dom.querySelector('a.my-purchase-list__link-item-detail').href;
			var prices = dom.querySelectorAll('.my-order-unit__info-ea');
			var totalPrice = Array.prototype.reduce.call(prices, function (prev, current) {
				current = parseFloat(current.innerText.replace(/(\d*개|\D)/g,''));
				return current + prev;
			}, 0);

			var names = dom.querySelectorAll('.my-order-unit__info-name strong');

			var order = {
				orderDate: new Date(dom.querySelector('h3').innerText.split(' ')[1]),
				orderNumber: detailUrl.match(/detail\/(\d*)/)[1],
				detailUrl: detailUrl,
				trackingUrl: trackingUrl,
				trackingNumber: trackingNumber, 	// accessible through trackingUrl
				carrier: carrier,			// accessible through trackingUrl
				status: dom.querySelector('.my-order-unit__status-roll-item strong').innerText,
				totalPrice: totalPrice,
				seller: null,
				//only first of multi orders
				image: dom.querySelector('img').src, 
				name: names[0].innerText + (names.length > 1 ? ' 외 ' + (names.length-1) + '개': ''),
			}
			return order;
		});

		return orders.filter(function(order) {
			var excluding = order.status.match(excludingStatusRegex);
			return !!!excluding;
		});
	}catch(e){
		return null;
	}
}
