/**
 * @module           Auction
 */

/**
 * 옥션 로그인
 * http://m.11st.co.kr/MW/Login/login.tmall?returnURL=http%253A%252F%252Fm.11st.co.kr%252FMW%252FMyPage%252ForderList.tmall
 *
 * @version    0.1.0
 *
 * @method     login
 * @param      {string}  id      login id
 * @param      {string}  pw      login password
 * @return     {Error}   페이지가 예상한것과 다를 경우 에러
 */
function login(id, pw){
  try{
    var loginForm = document.querySelector('div#hdivLoginForm');
    loginForm.querySelector('input#id').value=id;
    loginForm.querySelector('input#password').value=pw;
    loginForm.querySelector('button').click();
  }catch(e){
    return e;
  }
}

/**
 * 옥션 로그인 확인
 *
 * @version    0.1.0
 *
 * @method     checkLogin
 * @return     {bool}  로그인이 되었으면 true를 반환한다.
 */
function checkLogin(){
	try{
		var loginLogoutButton = document.querySelector('li.foot-item a')
		if(loginLogoutButton){ //Explicitly check for unexpected page update
			switch(loginLogoutButton.text){
				case '로그아웃':
					return true;
				case '로그인':
					return false;
				default:
					throw new Error('Login Check Failed');
			}
		}
		throw new Error('Login Check Failed');
	}catch(e){
		return e;
	}
}

/**
 * 옥션 주문 목록 조회 https://mmya.auction.co.kr/MyAuction/Order/#/OrderList
 *
 * @version    0.1.0
 *
 * @method     getOrders
 * @return     {Object[]}  주문 목록들
 */
function getOrders() {
	/*eslint-env es6*/
	var excludingStatuses = ['환불'];
	var excludingStatusRegex = new RegExp(`(?:${excludingStatuses.join('|')})`,'i');
	try { 
		var payments = angular.element('.order-products-list').scope().ui.orderList.PayInfoList
		var orders = []; 
		for(var paymentIndex = 0; paymentIndex < payments.length; paymentIndex++){
			var payment = payments[paymentIndex];
			var paymentOrderList = payment.OrderList;
			for(var orderIndex = 0; orderIndex < paymentOrderList.length; orderIndex++){
				var paymantOrder = paymentOrderList[orderIndex];
				var order = {
					orderDate: new Date(payment.PayDateString),
					orderNumber: paymantOrder.OrderNo,
					detailUrl: `https://mmya.auction.co.kr/MyAuction/Order/#/MyOrderDetail/${payment.PayNo}/${orderIndex}`,
					trackingUrl: paymantOrder.DisplayOption.DeliveryTrackingDisplay? `http://mobile.auction.co.kr/MyAuction/TraceItem.aspx?orderNo=${paymantOrder.OrderNo}` : null,
					trackingNumber: null, 
					carrier: null,
					image: paymantOrder.ItemImageUrl,
					name: paymantOrder.ItemName,
					status: paymantOrder.Status,
					price: {
						item: paymantOrder.ItemPrice,
						shipping: paymantOrder.ShippingFee
					},
					totalPrice: payment.TotalOrderAmount,
					seller: {
						name: paymantOrder.SellerNickName,
						tel: paymantOrder.SellerTelNumber,
						mobile: paymantOrder.SellerMobileTelNo,
						email: paymantOrder.SellerEmail,
						address: paymantOrder.SellerAddress
					}
				}
				var excluding = order.status.match(excludingStatusRegex);
				if(!excluding) orders.push(order);
			}
		}
		return orders;
	}catch(e){
		return e;
	}
}

/**
 * 옥션 배송 조회
 * http://mobile.auction.co.kr/MyAuction/TraceItem.aspx?orderNo=${currentOrder.OrderNo}
 *
 * @version    0.1.0
 *
 * @method     getTrackingInfo
 * @return     {Object | Error}  운송사와 운송장번호, 페이지가 예상한 것과 다르면 에러
 */
function getTrackingInfo() {
	try{
		var infoList = document.querySelectorAll('.delivery-company_info li strong'); //[carrier, trackingNumber]
		var order = {
			carrier: infoList[0].innerText,
			trackingNumber: infoList[1].innerText,
		}
		return order;
	}catch(e){
		return e;
	}
}

