# 쇼핑몰 구매목록 JS 저장소 및 간단 서버

## 설치
`npm install`

## 서버 실행
`npm start`

## Api Url
`http://localhost:3000/11st/login`

## Api 목록
- `[11st|auction|coupang|gmarket|wemakeprice]`/
    + `login`
    + `checkLogin`
    + `getOrders`
    + `getTrackingInfo`

## Api 반환 예시
`[#]ID[/#]`와 `[#]PW[/#]`를 치환하여 사용바람

### http://localhost:3000/11st/login
```javascript
var loginForm = document.querySelector('form#loginform');
loginForm.querySelector('input#userId').value='[#]ID[/#]';
loginForm.querySelector('input#userPw').value='[#]PW[/#]';
loginForm.querySelector('button#loginButton').click();
```

### http://localhost:3000/11st/getOrders
```javascript
/*eslint-env es6*/
var list = document.querySelector('#dataListAjax').querySelectorAll('section.odr_rbox');  //Explictly check #dataListAjax for 11st orderlist page
var orders = Array.prototype.map.call(list, function(dom) {
    var dnTexts = dom.querySelector('.odr_dn .dn').innerText.split(/\s+/); //[orderDate, ordernumber]
    var sellerTexts = dom.querySelector('.seller').innerText.split(/\s+/);
    var prices = dom.querySelectorAll('.info ul li b:first-of-type');
    var totalPrice = Array.prototype.reduce.call(prices, function (prev, current) {
        current = parseFloat(current.innerText.replace(',',''));
        return current + prev;
    }, 0);
    var trackingButton = Array.prototype.filter.call(dom.querySelectorAll('.prd_p .info .btn a'),(element, index, array)=>{return element.text == '배송추적'})[0];

    var order = {
        orderDate: new Date(dnTexts[0]),
        orderNumber: dnTexts[1],
        detailUrl: dom.querySelector('.odr_dn .dn a').href,
        trackingUrl: trackingButton? trackingButton.href : null,
        trackingNumber: null,   // accessible through trackingUrl
        carrier: null,          // accessible through trackingUrl
        image: dom.querySelector('.prd_p .thumb img').src,
        name: dom.querySelector('.prd_p .name').innerText,
        status: dom.querySelector('.prd_p .rt').innerText.replace(/\s*$/,''),
        totalPrice: totalPrice,
        seller: {
            name: sellerTexts[0],
            tel:sellerTexts[1]
        }
    }
    return order;
});
orders;
```